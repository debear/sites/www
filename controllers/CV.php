<?php

namespace DeBear\Http\Controllers\WWW;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Response as FrameworkResponse;
use Illuminate\Http\RedirectResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Merge;

class CV extends Controller
{
    /**
     * Render the online version of the CV
     * @return RedirectResponse|Response
     */
    public function index(): RedirectResponse|Response
    {
        // Redirect to HTTPS if requested over HTTP?
        if (!isset($_SERVER['HTTPS']) || !$_SERVER['HTTPS']) {
            return redirect('https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 308);
        }

        // Process some internals.
        HTML::setupAnalytics();
        $inc_ga = count(HTML::getAnalyticsUrchin());
        $nonce = FrameworkConfig::get('debear.headers.csp.nonce');
        $domain = '//debear.uk';
        $domain_static = HTTP::buildDomain('static');

        // Flags.
        if (!isset($_GET['slug']) && isset($_GET['utm_campaign'])) {
            $_GET['slug'] = $_GET['utm_campaign'];
        }
        $slug = (isset($_GET['slug']) && is_string($_GET['slug']) && $_GET['slug']
            ? filter_var($_GET['slug'], FILTER_SANITIZE_STRING)
            : false);
        $printing = isset($_GET['pdf']) && $_GET['pdf'];
        $cv_link = 'https:' . $domain . '/cv/' . ($slug ?: '');

        // Styles.
        $core_styles = ['cv/reset.css', 'cv/style.css', 'skel/flags/16.css', 'cv/screen.css', 'cv/print.css'];
        $fa_dir = 'skel/fontawesome';
        $fa_styles = ["$fa_dir/fontawesome.css", "$fa_dir/brands.css", "$fa_dir/regular.css", "$fa_dir/solid.css"];
        $css = array_merge($core_styles, !HTTP::isTest() ? $fa_styles : []);
        if (HTTP::isLive() || isset($_GET['merge-css'])) {
            $css = [ Merge::CSS($css) ];
        }

        // Build our response.
        $content = view('www/cv', compact([
            'inc_ga',
            'nonce',
            'domain',
            'domain_static',
            'slug',
            'printing',
            'cv_link',
            'css',
        ]));
        // Build our CSP headers.
        $csp_list = [
            "default-src 'none'",
            "script-src 'self' 'nonce-$nonce' https:$domain_static https://*.googletagmanager.com",
            "style-src 'self' https:$domain_static https://*.fontawesome.com",
            "connect-src 'self' https://*.googletagmanager.com https://*.google-analytics.com "
                . "https://*.analytics.google.com",
            "img-src 'self' https:$domain_static https://*.googletagmanager.com https://*.google-analytics.com",
            "font-src 'self' https:$domain_static",
            "form-action 'self'",
            "report-uri https:$domain/report-uri/csp",
        ];
        // Return with our headers.
        $response = FrameworkResponse::make($content, 200);
        HTTP::securityHeaders(
            $response,
            [
                // Content-Security-Policy (https://scotthelme.co.uk/content-security-policy-an-introduction/).
                'Content-Security-Policy' => join('; ', $csp_list),
                // Strict-Transport-Security (https://scotthelme.co.uk/hsts-the-missing-link-in-tls/).
                'Strict-Transport-Security' => 'max-age=31536000; includeSubDomains',
                // X-Frame-Options (https://scotthelme.co.uk/hardening-your-http-response-headers/#x-frame-options).
                'X-Frame-Options' => 'SAMEORIGIN',
                // X-XSS-Protection (https://scotthelme.co.uk/hardening-your-http-response-headers/#x-xss-protection).
                'X-XSS-Protection' => '1; mode=block',
                // X-Content-Type-Options
                // (https://scotthelme.co.uk/hardening-your-http-response-headers/#x-content-type-options).
                'X-Content-Type-Options' => 'nosniff',
                // Referrer-Policy (https://scotthelme.co.uk/a-new-security-header-referrer-policy/).
                'Referrer-Policy' => 'strict-origin-when-cross-origin',
            ],
            ['replace_default' => true]
        );
        return $response;
    }
}
