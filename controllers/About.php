<?php

namespace DeBear\Http\Controllers\WWW;

use Illuminate\Contracts\View\View;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;

class About extends Controller
{
    /**
     * List all the latest news articles
     * @return View
     */
    public function index(): View
    {
        // Custom page config.
        Resources::addCSS('pages/about.css');
        HTML::setPageTitle('About Us');
        HTML::setNavCurrent('/about');
        HTML::linkMetaDescription('about');

        // Then render.
        return view('www/about');
    }
}
