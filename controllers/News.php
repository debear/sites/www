<?php

namespace DeBear\Http\Controllers\WWW;

use Illuminate\Contracts\View\View;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Models\WWW\News as NewsModel;

class News extends Controller
{
    /**
     * List all the latest news articles
     * @return View
     */
    public function index(): View
    {
        // Custom page config.
        Resources::addCSS('pages/news.css');
        Resources::addJS('skel/widgets/articles.js');
        HTML::noHeaderLink();
        HTML::setNavCurrent('/');
        HTML::linkMetaDescription('home');

        // Pagination info.
        $num_articles = NewsModel::where('archived', 0)->count();
        $page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
        $per_page = 6;
        // Get all the articles ordered by creation date.
        $news = NewsModel::with(['creator', 'updator'])
            ->where('archived', 0)
            ->orderBy('created', 'desc')
            ->limit($per_page + ($page == 1 ? 1 : 0)); // Showing an extra item (the leader) on the homepage.
        // Paginating?
        if ($page > 1) {
            $news = $news->offset(($page - 1) * $per_page);
        }
        // Prepare to be passed to the view.
        $news = $news->get();

        return view('www/news', compact(['news', 'page', 'num_articles', 'per_page']));
    }
}
