<?php

namespace DeBear\Http\Controllers\WWW;

use Illuminate\Contracts\View\View;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Models\WWW\Sites as SitesModel;

class Sites extends Controller
{
    /**
     * List all the sites we have stored in the database
     * @return View
     */
    public function index(): View
    {
        // Custom page config.
        Resources::addCSS('pages/sites.css');
        Resources::addJS('pages/sites.js');
        HTML::setPageTitle('Our Sites');
        HTML::setNavCurrent('/sites');
        HTML::linkMetaDescription('sites');

        // Pagination info.
        $num_sites = SitesModel::count();
        $page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
        $per_page = 6;
        // Get all the articles ordered by creation date.
        $sites = SitesModel::with('screenshots')
            ->orderBy('launched', 'desc')
            ->limit($per_page);
        // Paginating?
        if ($page > 1) {
            $sites = $sites->offset(($page - 1) * $per_page);
        }
        // Prepare to be passed to the view.
        $sites = $sites->get();

        return view('www/sites', compact(['sites', 'page', 'num_sites', 'per_page']));
    }
}
