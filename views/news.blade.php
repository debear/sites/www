@extends('skeleton.layouts.html')

@section('content')

<h1>Welcome to {!! FrameworkConfig::get('app.name') !!}!</h1>

<ul class="inline_list clearfix news">

@foreach ($news as $article)

    <li class="item {!! $article->listClass($loop->index, $page) !!}">
        <article>
            <h3>{!! $article->headline !!}</h3>
            <p>{!! $article->render() !!}</p>
            <ul class="inline_list timestamps">
                <li>Created on {!! $article->created->format('l jS F Y') !!} by {!! $article->creator->name !!}</li>
                @if ($article->updated)
                    <li>Last Updated on {!! $article->updated->format('l jS F Y') !!} by {!! $article->updator->name !!}</li>
                @endif
            </ul>
        </article>
    </li>

@endforeach

</ul>

<div class="clearfix">
    {{-- Articles - 1 represents the extra article we display on the homepage --}}
    {!! Pagination::render($num_articles - 1, $per_page, '?page=', $page) !!}
</div>

@endsection
