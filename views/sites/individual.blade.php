<li class="item grid-1-2 grid-m-1-1 grid-t-1-1">
    <article>
        <h3>
            @if ($site->is_new)
                <span class="icon_new" title="New Website!">
            @endif
            {!! $site->name !!}
            @if ($site->active)
                - <a target="_blank" href="{!! $site->fullUrl() !!}">https:{!! $site->fullUrl() !!}</a>
            @endif
            @if ($site->is_new)
                </span>
            @endif
        </h3>
        @if ($img->count())
            <div class="img {!! $img->count() > 1 ? 'img_mult' : 'img_sngl' !!}">
                {{-- Require the inner skin only if we display thumbnails too --}}
                @if ($img->count() > 1)
                    <div class="img_inner">
                @endif

                <img id="thumb_{!! $i !!}" src="{!! $img[0]->fullUrl() !!}" alt="{!! $img[0]->descrip !!}" class="main">
                {{-- Additional thumbnails to display? --}}
                @if ($img->count() > 1)
                    <div class="thumbs">
                        @foreach ($img as $sub_img)
                            <a id="thumb_{!! $i !!}_img{!! $loop->index !!}" class="img onclick_target"><img class="thumb " src="{!! $sub_img->fullUrl() !!}" alt="{!! $sub_img->descrip !!}"></a>
                        @endforeach
                    </div>
                @endif

                {{-- Require the inner skin only if we display thumbnails too --}}
                @if ($img->count() > 1)
                    </div>
                @endif
            </div>
        @endif

        <p class="descrip">{!! $site->descrip !!}</p>
        <em>Launched: {!! $site->launched->format('l jS F Y') !!}</em>
    </article>
</li>
