@extends('skeleton.layouts.html')

@section('content')

<h1>{!! FrameworkConfig::get('app.name') !!} Websites</h1>

<ul class="inline_list clearfix sites">

    @foreach ($sites as $site)

        @include('www.sites.individual', ['i' => $loop->index, 'site' => $site, 'img' => $site->screenshots])

    @endforeach

</ul>

<div class="clearfix">
    {{-- Sites - 1 represents the extra article we display on the homepage --}}
    {!! Pagination::render($num_sites - 1, $per_page, '?page=', $page) !!}
</div>

@endsection
