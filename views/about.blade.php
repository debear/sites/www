@extends('skeleton.layouts.html')

@section('content')

<h1>About {!! FrameworkConfig::get('app.name') !!}</h1>

<p>As an avid Sports fan, and a keen player of the Fantasy Sports that accompany his favourite sports, Thierry Draper started {!! FrameworkConfig::get('app.name') !!} as a way to solve the personal challenge of creating his own Fantasy Sports website. The initial website was an attempt at creating a Fantasy Baseball game, but over time, the focus changed to try and create a "perfect world" solution where the same code was able to run the Fantasy games of multiple sports. Naturally, this proved to be a little <em>too</em> generic, and could not be considered succesful. Several iterations later &ndash; this time concentrating specifically on a single sport, (Ice) Hockey &ndash; and things went the other way: it became too sport specific, and therefore too un-manageable.</p>

<p class="tabbed_paragraph">The main focus of {!! FrameworkConfig::get('app.name') !!} will always be the creation and management of a Fantasy Sports website, including an accompanying Sports website to aid the Fantasy decision-making of each sport. However, as time has passed, other websites have been developed and launched that are not in keeping with this final goal &ndash; these websites, listed under the "<em>Our Sites</em>" page, have been mini-distractions created to improve certain aspects of the standard "skeleton" used in all {!! FrameworkConfig::get('app.name') !!} websites.</p>

<h2>Timeline</h2>

<dl class="timeline">
    <dt>Summer 2003</dt>
        <dd>Conception of website idea, Initial design and prototyping.</dd>
    <dt>Autumn 2004</dt>
        <dd>Major re-vamp of the design philosophy to generalise the end-product.</dd>
    <dt>Spring 2005</dt>
        <dd>Reverted to the sport-specific design philosophy.</dd>
    <dt>Summer 2005</dt>
        <dd>Initial design and implementation of standardised website interface.</dd>
    <dt>Autumn 2005</dt>
        <dd>Launch of Version 1.0 of the main {!! FrameworkConfig::get('app.name') !!} website, whilst still working on the various minisites.</dd>
    <dt>Spring 2006</dt>
        <dd>Launch of first minisite, DeBear Photos.</dd>
    <dt>Summer 2007</dt>
        <dd>Website re-design.</dd>
    <dt>Summer 2008</dt>
        <dd>Launch of first minisite after the design overhaul, DeBear Memorabilia.</dd>
    <dt>Autumn 2008</dt>
        <dd>{!! FrameworkConfig::get('app.name') !!} re-branded and re-launched.</dd>
    <dt>Spring 2010</dt>
        <dd><a target="_blank" href="{!! HTTP::buildDomain('sports') !!}/">DeBear Sports</a> launched.</dd>
    <dt>Spring 2011</dt>
        <dd>Launch of the very first <a target="_blank" href="{!! HTTP::buildDomain('fantasy') !!}/">DeBear Fantasy Sports</a> game, Formula One.</dd>
    <dt>Spring 2013</dt>
        <dd>Expand the proof-of-concept Formula One sites and launch both a MotoGP Sports sub-site and acccompanying Fantasy game.</dd>
    <dt>Autumn 2015</dt>
        <dd>Launch the first foray into North American sports, with the launch of <a target="_blank" href="{!! HTTP::buildDomain('sports') !!}/ahl/">AHL</a>, <a target="_blank" href="{!! HTTP::buildDomain('sports') !!}/nhl/">NHL</a> and <a target="_blank" href="{!! HTTP::buildDomain('sports') !!}/nfl/">NFL</a> Sports sub-sites.</dd>
    <dt>Summer 2016</dt>
        <dd>First Major League fantasy site launched, <a target="_blank" href="{!! HTTP::buildDomain('fantasy') !!}/nfl-cap">DeBear Fantasy NFL Salary Cap</a>.</dd>
    <dt>Summer 2017</dt>
        <dd>Culmination of an 18 month project, the launch of <a target="_blank" href="{!! HTTP::buildDomain('sports') !!}/mlb">DeBear Sports MLB</a>.</dd>
    <dt>Summer 2018</dt>
        <dd>Underlying engine of the platform re-factored to improve future scalability.</dd>
</dl>

<h2>Thierry Draper</h2>

<img class="profile" src="{!! HTTP::buildDomain('static') !!}/{!! FrameworkConfig::get('debear.url.sub') !!}/{!! FrameworkConfig::get('debear.dirs.images') !!}/thierry.png" alt="Thierry Draper">

<p>A keen sports fan &ndash; and a not-so-able sportsman during the "glory days" of his school sporting career &ndash; Thierry Draper grew up on a staple diet of Rugby Union, Football (Soccer) and, drawing on his French roots, Cycling. Throw in to the mix sports like Cricket and Tennis played during Physical Education lessons at school, and it's pretty obvious Thierry grew up with sport in his veins. However, it wasn't until a chance encounter in 1997 that sport really took hold of his life: <em>Blitz</em> &ndash; Channel 4's weekly highlight show of the previous weeks NFL action. Up until this point, Thierry had always taken the stereotypically British point of view on American Football: a pointless stop-start sport played by over-dressed idiots.</p>

<p class="tabbed_paragraph">However, after watching <em>Blitz</em> one Saturday afternoon (in no-small part because it was presented by <a rel="noopener noreferrer" target="_blank" href="//en.wikipedia.org/wiki/Gary_Imlach">Gary Imlach</a>, a favourite presenter of Thierry's), he started to appreciate the tactics of the sport and understand "the point of it". After watching more and more of the sport, he slowly got into it to such an extent that he viewed it as one of his favourite sports (as evidenced by the photo on the left, taken in 2007 at the first <a rel="noopener noreferrer" target="_blank" href="//en.wikipedia.org/wiki/NFL_International_Series">Regular Season NFL game</a> to be played outside of North America) and slowly started following other North American sports, starting with the <a rel="noopener noreferrer" target="_blank" href="//en.wikipedia.org/wiki/2000_World_Series">2000 Baseball World Series</a> and quickly moving on to (Ice) Hockey and the <a rel="noopener noreferrer" target="_blank" href="//en.wikipedia.org/wiki/2001_Stanley_Cup_Finals">2001 Stanley Cup Finals</a>.</p>

<p class="tabbed_paragraph">With improvements to internet access and a move to University, Thierry started playing Fantasy Sports &ndash; by this time a popular activity associated with North American sports &ndash; starting with American Football, but eventually including (Ice) Hockey, Baseball and many other sports on a regular basis. With a history in computers and of programming in both his spare time and for his University course, Thierry set out the personal challenge of combining his two hobbies: creating his own Fantasy Sports website.</p>

@endsection
