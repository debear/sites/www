<html>
<head>
    <meta name="Description" content="Curriculum Vitae &ndash; Thierry Draper" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@thierrydraper" />
    <meta name="twitter:title" content="DeBear.uk" />
    <meta name="twitter:description" content="Curriculum Vitae &ndash; Thierry Draper" />
    <meta name="twitter:creator" content="@thierrydraper" />
    <meta name="twitter:image" content="//{!! FrameworkConfig::get('debear.url.subdomains.www') !!}/{!! FrameworkConfig::get('debear.dirs.images') !!}/meta/debear.png" />
    <meta property="og:title" content="DeBear.uk" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="//{!! FrameworkConfig::get('debear.url.subdomains.www') !!}/" />
    <meta property="og:image" content="//{!! FrameworkConfig::get('debear.url.subdomains.www') !!}/{!! FrameworkConfig::get('debear.dirs.images') !!}/meta/debear.png" />
    <meta property="og:image:width" content="300" />
    <meta property="og:image:height" content="300" />
    <meta property="og:description" content="Curriculum Vitae &ndash; Thierry Draper" />
    <meta property="og:site_name" content="DeBear.uk" />
    <meta name="theme-color" content="#4682B4" />
    <title>Curriculum Vitae &ndash; Thierry Draper</title>
    @foreach ($css as $f)
        @php
            if (substr($f, 0, 5) == 'skel/') {
                $d = 'skel';
                $f = substr($f, 5);
            } else {
                $d = FrameworkConfig::get('debear.url.sub');
            }
        @endphp
        <link rel="stylesheet" type="text/css" href="{!! $domain_static !!}/{!! $d !!}/css/{!! $f !!}" />
    @endforeach
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
    @if ($inc_ga)
        @foreach (['input', 'analytics'] as $js)
            <script src="{!! $domain_static !!}/skel/js/{!! $js !!}.js" nonce="{!! $nonce !!}"></script>
        @endforeach
        @include('skeleton.layouts.html.analytics')
    @elseif ($slug)
        <!-- GA disabled, but identified the slug '{!! $slug !!}' -->
    @endif
</head>
<body>
    <h1>Thierry Draper</h1>

    <div class="clearfix info">
        <dl class="details">
            @if ($printing)
                <dt><i class="fas fa-mobile-alt"></i>Telephone:</dt>
                    <dd>07967 118889</dd>
            @endif
            <dt><i class="far fa-envelope"></i>Email:</dt>
                <dd><span class="enom">hello</span><span class="edenom">debear.uk</span></dd>
            <dt><i class="fas fa-map-signs"></i>Nationality:</dt>
                <dd class="flag flag16_gb-cym">British</dd>
                <dd class="flag flag16_fr">French</dd>
        </dl>

        <dl class="skills">
            {!! CMS::getSection('/cv', 'sidebar-list') !!}
        </dl>

        <dl class="links">
            <dt>External Links</dt>
                <dd><a class="no-print" href="//debear.uk/?utm_source=DeBear&utm_medium=CV&utm_campaign={!! $slug ?: 'default' !!}" target="_blank"><i class="fas fa-globe" title="Personal Website"></i>https://debear.uk</a></dd>
                {!! CMS::getSection('/cv', 'online-presence') !!}
        </dl>
    </div>

    <div class="section tldr">
        {!! CMS::getSection('/cv', 'tl;dr') !!}
    </div>

    <div class="section empl">
        <h2>Employment History</h2>
        <dl class="empl">
            {!! CMS::getSection('/cv', 'employment-history') !!}
        </dl>
    </div>

    <div class="section personal">
        <h2>Education &amp; Qualifications</h2>
        <dl class="edu">
            <dt>University of Bath</dt>
                <dd>2:2 &ndash; BSc (Hons) Computer Science with Industrial Placement</dd>
            <dt>Brynteg Comprehensive School, Bridgend</dt>
                <dd>A-Levels:</dd>
                <dd>1 A &ndash; Mathematics</dd>
                <dd>2 B &ndash; Further Mathematics, Computing</dd>
                <dd>1 D &ndash; Physics</dd>
        </dl>

        <h2>Other Positions of Responsibility</h2>
        <dl class="resp">
            {!! CMS::getSection('/cv', 'responsibilities') !!}
        </dl>

        <h2>Hobbies</h2>
        <dl class="hobbies">
            {!! CMS::getSection('/cv', 'hobbies') !!}
        </dl>
    </div>

    <div class="section clearfix references">
        <h2>Referees</h2>
        @if ($printing)
            <dl class="half">
                {!! CMS::getSection('/cv', 'referee-left') !!}
            </dl>
            <dl class="half">
                {!! CMS::getSection('/cv', 'referee-right') !!}
            </dl>
        @else
            <em>Available upon request</em>
        @endif
    </div>

    <div class="online_cv print_only">
        An online version of this CV can be found at <a href="{!! $cv_link !!}" target="_blank">{!! $cv_link !!}</a>.
    </div>
</body>
</html>
