# DeBear.uk Homesite

A showcase of all the sub-sites with a make-shift "news" section to highlight new projects we're working on and when they launch.

## Status

[![Version: 2.0](https://img.shields.io/badge/Version-2.0-brightgreen.svg)](https://gitlab.com/debear/www/blob/master/CHANGELOG.md)
[![Build: 267](https://img.shields.io/badge/Build-267-yellow.svg)](https://gitlab.com/debear/www/blob/master/CHANGELOG.md)
[![Skeleton: 1432](https://img.shields.io/badge/Skeleton-1432-orange.svg)](https://gitlab.com/debear/skeleton)
[![Coding Style: PSR-12](https://img.shields.io/badge/Coding_Style-PSR--12-lightgrey.svg)](https://github.com/php-fig/fig-standards/blob/master/proposed/extended-coding-style-guide.md)
[![Pipeline Status](https://gitlab.com/debear/www/badges/master/pipeline.svg)](https://gitlab.com/debear/www/commits/master)
[![Coverage Report](https://gitlab.com/debear/sites/www/badges/master/coverage.svg)](https://gitlab.com/debear/sites/www/commits/master)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

## Motivation

In all honesty, what else could the homepage be? It's unfortunately the afterthought it looks like.

## Authors

* **Thierry Draper** - [@ThierryDraper](https://gitlab.com/thierrydraper)

## Contribute

Please read [CONTRIBUTING.md](https://gitlab.com/debear/www/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is made available under the terms of the GNU Affero General Public License v3.0. Please see the [LICENSE](https://gitlab.com/debear/www/blob/master/LICENSE) file for details.

GNU AGPLv3 © [Thierry Draper](https://gitlab.com/thierrydraper)
