## Build v2.0.267 - f6219ec - 2025-02-22 - SoftDep: February 2025
* f6219ec: Update PHP.ini locations to the new docker image location (_Thierry Draper_)
* c8b1fb2: Migrate a response generator exception to the framework Response class (_Thierry Draper_)
* 4046ad1: Migrate config getting to the framework Config class (_Thierry Draper_)

## Build v2.0.264 - bd80b2d - 2025-01-17 - SoftDep: January 2025
* (Auto-commits only)

## Build v2.0.264 - 7f09aa3 - 2024-12-15 - SoftDep: December 2024
* (Auto-commits only)

## Build v2.0.264 - 679f970 - 2024-11-16 - SoftDep: November 2024
* (Auto-commits only)

## Build v2.0.264 - 35262d7 - 2024-10-21 - SoftDep: October 2024
* (Auto-commits only)

## Build v2.0.264 - 67b74f6 - 2024-09-15 - SoftDep: September 2024
* (Auto-commits only)

## Build v2.0.264 - 3b0643b - 2024-09-15 - SysAdmin: CI Tweaks
* 3b0643b: Catch Phan run errors that could cause cyclical self-calling (_Thierry Draper_)
* c3b8b82: Fix first run setup for downloading seed data (_Thierry Draper_)
* 9bd544a: Apply some minor CI script tidying and improvements (_Thierry Draper_)

## Build v2.0.261 - abea137 - 2024-08-19 - SoftDep: August 2024
* (Auto-commits only)

## Build v2.0.261 - a90c843 - 2024-08-09 - SysAdmin: MariaDB to MySQL Migration
* a90c843: Migrate our CI from MariaDB to MySQL (_Thierry Draper_)

## Build v2.0.260 - e657001 - 2024-07-17 - Hotfix: SAST Reporting
* e657001: Fix the SAST report download sort ordering (_Thierry Draper_)

## Build v2.0.259 - b2a91db - 2024-07-17 - Feature: CI Server Sync Job
* b2a91db: Include the server sync database to our CI job list (_Thierry Draper_)

## Build v2.0.258 - ab52c1c - 2024-07-16 - SoftDep: July 2024
* (Auto-commits only)

## Build v2.0.258 - 50e3826 - 2024-06-21 - Hotfix: Local CI Job Parsing
* 50e3826: Fix the local CI job parser to exclude comments after the allow_failure flag (_Thierry Draper_)

## Build v2.0.257 - 2f194d4 - 2024-06-18 - SoftDep: June 2024
* (Auto-commits only)

## Build v2.0.257 - c94fe5b - 2024-06-17 - Hotfix: CI Downloads
* c94fe5b: Fix the request sent to determine what CI data files need downloading (_Thierry Draper_)
* 77e304d: Improve the location of the CI download trust store cert (_Thierry Draper_)

## Build v2.0.255 - 39da045 - 2024-05-18 - SoftDep: May 2024
* (Auto-commits only)

## Build v2.0.255 - dbecd0c - 2024-05-13 - Security: CI Jobs
* dbecd0c: Switch our localci SAST exclusion to the new emulator (_Thierry Draper_)
* f66f690: Implement a local SAST emulator script using the GitLab docker images (_Thierry Draper_)
* 5cceb46: Implement an initial SAST reporting tool (_Thierry Draper_)
* b169d06: Migrate our only/except job logic to the preferred rules format (_Thierry Draper_)
* 029d258: Incorporate the GitLab managed SAST pipeline job (_Thierry Draper_)
* b286321: Add some native tool dependency vulnerability scanning within our CI (_Thierry Draper_)

## Build v2.0.249 - 282e8da - 2024-04-27 - Improvement: CI Downloads
* 282e8da: Switch CI downloads from wget to curl (_Thierry Draper_)

## Build v2.0.248 - f3b31e0 - 2024-04-24 - Improvement: JavaScript Formatting
* f3b31e0: Disable the eslint unused disabled rule checked whilst linting, as it conflicts with the standards check (_Thierry Draper_)
* c1cc312: Migrate to eslints flat config format (_Thierry Draper_)

## Build v2.0.246 - 87b303f - 2024-04-21 - SoftDep: April 2024
* (Auto-commits only)

## Build v2.0.246 - bd7ee43 - 2024-04-18 - Improvement: CI ECMAScript Version
* bd7ee43: Update the targeted version of ECMAScript in CI to v13 / 2022 (_Thierry Draper_)

## Build v2.0.245 - 9db93a7 - 2024-03-17 - SoftDep: Laravel 11 and March 2024 Update
* 9db93a7: Upgrade dependencies to PHPUnit 11 (_Thierry Draper_)
* 701e290: Switch use of the now-removed $dates property in Eloquent to its $casts equivalent (_Thierry Draper_)
* 7d242b1: Upgrade dependencies to PHPUnit 10 (_Thierry Draper_)

## Build v2.0.242 - a4ed54c - 2024-02-17 - SoftDep: February 2024
* (Auto-commits only)

## Build v2.0.242 - 2977bdd - 2024-01-27 - Hotfix: CSS Class Scope
* 2977bdd: Tighten a too loosely defined CSS class scope (_Thierry Draper_)

## Build v2.0.241 - f09f83f - 2024-01-27 - Improvement: Refactoring JavaScript
* f09f83f: Replace the JavaScript linter with something less enforcing of (clunky) standards (_Thierry Draper_)
* c6ce7b8: Move to greater use of the arrow functions (_Thierry Draper_)
* da80e2f: Move away from the deprecated centralised DOM querySelector parent node argument (_Thierry Draper_)
* 40dad06: Update article rendering following skeleton updates to JavaScript event handling (_Thierry Draper_)
* 60c631e: Update the CI jobs now we have JavaScript to include in the repo (_Thierry Draper_)
* a69d110: Move site list JavaScript events from server-side (rendered in the HTML) to client-side (_Thierry Draper_)

## Build v2.0.235 - bc9f62b - 2024-01-19 - Improvement: JavaScript Null Operators
* bc9f62b: Update eslints base ECMAScript version to allow for null-handling simplification (_Thierry Draper_)

## Build v2.0.234 - 5220472 - 2024-01-14 - SoftDep: January 2024
* (Auto-commits only)

## Build v2.0.234 - 7ab8e4a - 2023-12-17 - SoftDep: December 2023
* (Auto-commits only)

## Build v2.0.234 - 52ae048 - 2023-11-27 - CI: Standardised CSS Standards
* 52ae048: Apply our standard 120-char width for CSS standards rather than the default 80 (_Thierry Draper_)

## Build v2.0.233 - a8abefb - 2023-10-14 - Composer: October 2023
* a8abefb: Backport a CI fix from earlier skeleton CDN update (_Thierry Draper_)

## Build v2.0.232 - 2eb87b9 - 2023-09-18 - Composer: September 2023
* (Auto-commits only)

## Build v2.0.232 - 118f9c9 - 2023-08-30 - Improvement: CI SymLink Conversion
* 118f9c9: Switch to the new symlink-to-dir CI converter (_Thierry Draper_)

## Build v2.0.231 - d61c7a0 - 2023-08-07 - Hotfix: Site Archiving
* d61c7a0: Fix the active flag removing an archived site from pagination calculations (_Thierry Draper_)

## Build v2.0.230 - 7bc7bac - 2023-08-05 - Composer: August 2023
* (Auto-commits only)

## Build v2.0.230 - 330bd85 - 2023-07-15 - Composer: July 2023
* (Auto-commits only)

## Build v2.0.230 - 299d531 - 2023-06-17 - Composer: June 2023
* (Auto-commits only)

## Build v2.0.230 - da6152e - 2023-05-14 - Composer: May 2023
* (Auto-commits only)

## Build v2.0.230 - 8a607f6 - 2023-05-02 - SysAdmin: Deprecations
* 8a607f6: Replace use of fgrep with grep given it is being flagged overtly as obsolete (_Thierry Draper_)

## Build v2.0.229 - c39e5b3 - 2023-04-15 - Composer: April 2023
* (Auto-commits only)

## Build v2.0.229 - 01eb774 - 2023-03-11 - Composer: March 2023
* (Auto-commits only)

## Build v2.0.229 - 7bf955d - 2023-03-01 - Improvement: CSS Standards to Prettier
* 7bf955d: Fix CSS standards according to the new Prettier rules (_Thierry Draper_)
* c4e1f4e: Switch the deprecated Stylelint CSS standards to Prettier (_Thierry Draper_)

## Build v2.0.227 - aa3c2c3 - 2023-02-12 - Composer: February 2023
* (Auto-commits only)

## Build v2.0.227 - dc994a3 - 2023-01-15 - Composer: January 2023
* (Auto-commits only)

## Build v2.0.227 - 1e9bac6 - 2023-01-08 - SysAdmin: CI Docker Images from Container Registry
* 1e9bac6: Switch the CI jobs to images from our Container Registry (_Thierry Draper_)

## Build v2.0.226 - b28dba3 - 2023-01-05 - Improvement: MySQL Linting
* b28dba3: Switch the CI MariaDB collation to latin1_general (_Thierry Draper_)
* f3b9b5d: Include the CI job for linting setup scripts (_Thierry Draper_)

## Build v2.0.224 - 7206503 - 2022-12-18 - Composer: December 2022
* (Auto-commits only)

## Build v2.0.224 - 4c4f09a - 2022-11-19 - Composer: November 2022
* (Auto-commits only)

## Build v2.0.224 - f4a2e43 - 2022-11-15 - Improvement: Switch Analytics to GA4
* f4a2e43: Switch Google Analytics to dual-running of UA (v3) and GA4 (_Thierry Draper_)

## Build v2.0.223 - ef04c2c - 2022-10-16 - Composer: October 2022
* (Auto-commits only)

## Build v2.0.223 - 342c3a2 - 2022-09-18 - Composer: September 2022
* (Auto-commits only)

## Build v2.0.223 - e8521f4 - 2022-09-14 - CI: Environment dependency fix
* e8521f4: Remove the upstream loading of font awesome, that is no longer required (_Thierry Draper_)
* 7d4dbcc: Install wget as a CI environment dependency (_Thierry Draper_)

## Build v2.0.221 - c7a23f5 - 2022-08-14 - Composer: August 2022
* (Auto-commits only)

## Build v2.0.221 - d40e92e - 2022-07-19 - SysAdmin: Backport for PHP 8.0
* d40e92e: Update the composer file to allow PHP 8.0, as well as the previous 8.1 (_Thierry Draper_)

## Build v2.0.220 - c584522 - 2022-07-16 - Composer: July 2022
* (Auto-commits only)

## Build v2.0.220 - d385c7a - 2022-07-15 - Hotfix: PHPMD TooManyMethods whitelist
* d385c7a: Increase the scope of PHPMDs TooManyMethods whitelist (_Thierry Draper_)

## Build v2.0.219 - 1baadb0 - 2022-06-19 - Composer: June 2022
* (Auto-commits only)

## Build v2.0.219 - 1bac49d - 2022-06-09 - Improvement: Latest CV tweaks
* 1bac49d: Apply the latest CV tweaks (_Thierry Draper_)

## Build v2.0.218 - d2e54f9 - 2022-06-08 - SysAdmin: Revert to native Blade data displaying
* d2e54f9: Re-factor our Blade template variable echoing to use the appropriate native Laravel mechanism (_Thierry Draper_)
* 152b8fe: Move common DeBear helper classes to the alias list for use in Blade templates (_Thierry Draper_)
* 1df974f: Revert use of @print and @config in Blade templates with the native Laravel method (_Thierry Draper_)
* 7c1010a: Restore Blade linting using the new dependency (_Thierry Draper_)

## Build v2.0.214 - 8004830 - 2022-05-27 - CI: PHP Code Coverage streamlining
* 8004830: Streamline the PHP Code Coverage output, and consider output less than 100% as a failure (_Thierry Draper_)

## Build v2.0.213 - 7dea38b - 2022-05-26 - CI: Report PHP Unit code coverage on master
* 7dea38b: Propagate the PHP Unit code coverage report to the deploy stage for its result to be tagged against the master branch (_Thierry Draper_)

## Build v2.0.212 - a6545d8 - 2022-05-22 - CI: Switch Code Coverage Driver
* a6545d8: Switch Code Coverage from using XDebug to PCOV (_Thierry Draper_)

## Build v2.0.211 - c031716 - 2022-05-22 - Improvement: Unit Tests as part of Code Coverage
* c031716: Add the GitLab code coverage regexp pattern, previously configured within the UI (_Thierry Draper_)
* 326dc90: Refine our Unit Tests according to localci tweaks made to the skeleton (_Thierry Draper_)
* b05bc25: Include appropriate PHPUnit code in our code coverage tests (_Thierry Draper_)

## Build v2.0.208 - b957253 - 2022-05-22 - Improvement: Laravel 9
* b957253: Remove Blade linting that is no longer available following the Laravel 9 upgrade (_Thierry Draper_)

## Build v2.0.207 - f3f6a45 - 2022-05-21 - SysAdmin: PHP 8
* 6d262eb: Use a new stylelint rule modifier to prevent recent false negatives (_Thierry Draper_)
* 85b80c7: Add a new CI step to lint Blade templates (_Thierry Draper_)
* 4628165: Move away from our Arrays::hasIndex wrapper (_Thierry Draper_)
* d4d479e: Implement PHP 8s new union param/return types (_Thierry Draper_)
* 2d564ce: Include a repo-specific version of the Phan PHP Standards CI job (_Thierry Draper_)
* b7395e0: Apply the final fixes suggested by Phan (_Thierry Draper_)
* 454db76: Apply several type-hinting related fixes (_Thierry Draper_)
* 9c642f7: Remove unused imported classes (_Thierry Draper_)
* ae36155: Switch from Laravel's on-the-fly class facades at the root level to their actual full path (_Thierry Draper_)

## Build v2.0.198 - d47939b - 2021-11-20 - Hotfix: Middleware Routing
* d47939b: Switch to the correct method for per-request middleware customisation (_Thierry Draper_)

## Build v2.0.197 - 354ccd8 - 2021-11-19 - Hotfix: CV Font Awesome CSP header
* 354ccd8: Ensure the CV page is using the correct Font Awesome CSP headers (_Thierry Draper_)

## Build v2.0.196 - 95da084 - 2021-11-13 - Composer: November 2021
* (Auto-commits only)

## Build v2.0.196 - fe75fe5 - 2021-11-12 - CI: CSS Property Standards
* fe75fe5: Update CSS property rules to reflect the new alphabetical standards (_Thierry Draper_)
* f76eb0b: Add new stylelint standards test for alphabetical CSS property ordering (_Thierry Draper_)
* 828d217: Remove a recently deprecated stylelint rule (_Thierry Draper_)

## Build v2.0.193 - 3153d56 - 2021-10-16 - Composer: October 2021
* (Auto-commits only)

## Build v2.0.193 - bed0b11 - 2021-09-18 - Composer: September 2021
* (Auto-commits only)

## Build v2.0.193 - c28e29b - 2021-08-13 - Composer: August 2021
* (Auto-commits only)

## Build v2.0.193 - a4aa720 - 2021-08-13 - Hotfix: PHPUnit XML .env spec
* a4aa720: Add missing .env values to the PHPUnit test XML spec (_Thierry Draper_)

## Build v2.0.192 - 22f56b0 - 2021-07-27 - Feature: Sitemap URL tester
* 22f56b0: Sitemap parser and processor for URL testing (_Thierry Draper_)

## Build v2.0.191 - d34ce06 - 2021-07-17 - Composer: July 2021
* (Auto-commits only)

## Build v2.0.191 - 2dd36d5 - 2021-06-21 - Composer: June 2021
* (Auto-commits only)

## Build v2.0.191 - 7913fc3 - 2021-05-15 - Composer: May 2021
* (Auto-commits only)

## Build v2.0.191 - 59bc4c8 - 2021-04-30 - CI: Pipeline Tweaks
* 59bc4c8: Make use of the CI's needs: option to streamline job and stage links (_Thierry Draper_)

## Build v2.0.190 - f08a5ce - 2021-04-17 - Composer: April 2021
* (Auto-commits only)

## Build v2.0.190 - 3602977 - 2021-03-13 - Composer: March 2021
* 3602977: Apply mobile pagination skeleton fixes to the CI (_Thierry Draper_)

## Build v2.0.189 - 97bb920 - 2021-02-13 - Composer: February 2021
* (Auto-commits only)

## Build v2.0.189 - 7631777 - 2021-01-16 - Composer: January 2021
* (Auto-commits only)

## Build v2.0.189 - d94f44e - 2021-01-11 - Improvement: Password Policy Skeleton Changes
* d94f44e: Remove an unused class import on the News model (_Thierry Draper_)
* c3f7343: Rely on an appropriate version of guzzlehttp/guzzle after adding the password policy (_Thierry Draper_)
* 65919e2: Start using the new @config Blade directive for rending config values (_Thierry Draper_)

## Build v2.0.186 - ed25390 - 2021-01-03 - CI: Only deploy on master branch
* ed25390: Only deploy during CI/AD of the master branch (_Thierry Draper_)

## Build v2.0.185 - 528101b - 2020-12-20 - Composer: December 2020
* (Auto-commits only)

## Build v2.0.185 - f60512f - 2020-12-05 - Improvement: Move to Xdebug 3
* f60512f: Fixes for the switch from Xdebug 2 to 3 (_Thierry Draper_)

## Build v2.0.184 - 0fad4a3 - 2020-11-14 - Composer: November 2020
* (Auto-commits only)

## Build v2.0.184 - 5cc0822 - 2020-10-16 - Improvement: Unit Test manifest.json
* 5cc0822: Add the manifest.json file to the suite of Unit Tests (_Thierry Draper_)

## Build v2.0.183 - 6e3318d - 2020-10-09 - Composer: Upgrade to Laravel 8
* 6e3318d: Update our CI rules following the Laravel 8 (+deps) upgrade (_Thierry Draper_)
* 31cb859: Update dependencies from upgrading from Laravel 7 to 8 (_Thierry Draper_)

## Build v2.0.181 - fdd342d - 2020-09-26 - Improvement: Migrations to Schema
* fdd342d: Switch from Migrations to Schemas updated via schema-sync (_Thierry Draper_)

## Build v2.0.180 - c057f78 - 2020-09-19 - Composer: September 2020
* (Auto-commits only)

## Build v2.0.180 - bff338a - 2020-09-19 - CI: Merge PHP Standards Jobs
* bff338a: Minor CI Code Coverage improvements (_Thierry Draper_)
* 895528e: Merge the PHP Mess Detector and Standards tests into a single job (_Thierry Draper_)

## Build v2.0.178 - 0f5197d - 2020-08-15 - Composer: August 2020
* (Auto-commits only)

## Build v2.0.178 - 92bf006 - 2020-07-13 - Composer: July 2020
* (Auto-commits only)

## Build v2.0.178 - f7d9253 - 2020-06-17 - Hotfix: PHPUnit Test Locale
* f7d9253: Fix PHPUnit tests on CI as the locale does not match dev/prod environments (_Thierry Draper_)

## Build v2.0.177 - a8b174d - 2020-06-14 - Composer: June 2020
* (Auto-commits only)

## Build v2.0.177 - 28f7c79 - 2020-06-13 - SysAdmin: Reduce Release Overlap
* 28f7c79: Reduce the overlap time between releases when performing the release (_Thierry Draper_)

## Build v2.0.176 - e35d9f3 - 2020-06-06 - Improvement: PSR-4-like Standards Check
* e35d9f3: Add PSR-4 like standards checks, given recent class and file name mis-matches (_Thierry Draper_)

## Build v2.0.175 - d3635c0 - 2020-06-02 - CI: Local PHPUnit Dev Setup
* d3635c0: Add scripts to setup a local dev environment for working on PHPUnit tests (_Thierry Draper_)

## Build v2.0.174 - f3fefd7 - 2020-05-18 - Composer: May 2020
* (Auto-commits only)

## Build v2.0.174 - 1960080 - 2020-04-21 - Composer: April 2020
* (Auto-commits only)

## Build v2.0.174 - 5b19142 - 2020-04-07 - Improvement: Dynamic PHPUnit Setup Components
* 5b19142: Standarise and split the PHPUnit setup script into dynamic components (_Thierry Draper_)

## Build v2.0.173 - 3648f1e - 2020-04-06 - Hotfix: PHPUnit Error Handling
* 3648f1e: Ensure the new PHPUnit run script errors on failure at the appropriate stage (_Thierry Draper_)

## Build v2.0.172 - 9e902fb - 2020-04-04 - Composer: Replacing PHP Linter
* 9e902fb: Replace a deprecated PHP Linting package with its replacement (_Thierry Draper_)

## Build v2.0.171 - a790542 - 2020-04-04 - Improvement: PHPUnit Efficiency
* a790542: Split the PHPUnit run in to a more efficient process (_Thierry Draper_)

## Build v2.0.170 - 469534e - 2020-03-14 - Composer: March 2020
* (Auto-commits only)

## Build v2.0.170 - 619f048 - 2020-02-14 - Composer: February 2020
* (Auto-commits only)

## Build v2.0.170 - 89ddfdf - 2020-02-03 - Improvement: Simplified Sitemap
* 89ddfdf: Merge the sitemap and nav config sfollowing our simplification step (_Thierry Draper_)

## Build v2.0.169 - 64e3b0c - 2020-01-31 - SysAdmin: .gitignore for env config
* 64e3b0c: Updated gitignore to hide a local env config file (_Thierry Draper_)

## Build v2.0.168 - 7e83ca8 - 2020-01-12 - Composer: January 2020
* (Auto-commits only)

## Build v2.0.168 - dd5e2bb - 2019-12-31 - SysAdmin: Prune SCHEMA server-sync steps
* dd5e2bb: Prune the code/schema server-sync steps we've new mechanisms for (_Thierry Draper_)

## Build v2.0.167 - cde7095 - 2019-12-26 - Hotfix: PHPUnit CI Job Shell Errors
* cde7095: Fix tput/clear errors during the PHPUnit CI job (_Thierry Draper_)

## Build v2.0.166 - cf43ff0 - 2019-12-15 - Composer: December 2019
* (Auto-commits only)

## Build v2.0.166 - 9ddfca7 - 2019-11-19 - CI: PHP Comments Standards
* 9ddfca7: Fall-out from the new PHP comments standards being enforced (_Thierry Draper_)
* 0e9352c: Extend our PSR-12 standards check to include the format of comments (_Thierry Draper_)

## Build v2.0.164 - 3f1dabf - 2019-11-16 - Composer: November 2019
* (Auto-commits only)

## Build v2.0.164 - d42e160 - 2019-11-14 - CI: YAML Fixes and Improvements
* d42e160: CI YAML fixes and improvements, such as moving the PHP/MySQL versions used to GitLab (_Thierry Draper_)

## Build v2.0.163 - fc52edf - 2019-10-23 - Composer: October 2019
* fc52edf: PSR-12 whitespace fixes (_Thierry Draper_)

## Build v2.0.162 - 5d7972c - 2019-10-19 - Hotifx: CSS Standards CI Stage
* 5d7972c: Fix the CSS Standards CI job so it runs in the correct group (_Thierry Draper_)

## Build v2.0.161 - b33780f - 2019-10-18 - Improvement: CSS Linting
* b33780f: CSS tweaks from new linting and standards checks (_Thierry Draper_)
* 912a155: Switch the CSS Linting test to use stylelint, and split in to both linting and standards (_Thierry Draper_)

## Build v2.0.159 - 77a0dab - 2019-09-25 - Composer: September 2019
* (Auto-commits only)

## Build v2.0.159 - 1d2c1c7 - 2019-09-02 - CI: Fix 'Clean' Local Runs
* 1d2c1c7: Fix the 'clean' running of our CI locally (_Thierry Draper_)

## Build v2.0.158 - 2f08243 - 2019-08-27 - Composer: August 2019
* (Auto-commits only)

## Build v2.0.158 - af80899 - 2019-08-24 - Improvement: User's Name As Accessor
* af80899: Switch the user's name concatenator to an accessor (_Thierry Draper_)

## Build v2.0.157 - 2c71265 - 2019-08-22 - Hotfix: Google Analytics Config Key
* 2c71265: Fix the Google Analytics config key so the correct Urchin is used (_Thierry Draper_)

## Build v2.0.156 - 10a274e - 2019-08-19 - Improvement: Load Font Awesome via Kits
* 10a274e: Updated CSP rules following the switch to Font Awesome's new kits (_Thierry Draper_)

## Build v2.0.155 - cfd49cc - 2019-08-12 - CI: 100% Code Coverage
* cfd49cc: CI use of merged directories actually needs the symlinks converted to usable dirs (_Thierry Draper_)
* 9ff0f1d: Need (temporary) CSS/JS merging directory in our CI environment (_Thierry Draper_)
* 1e0c27e: Remove deploy Unit Test file we are not using (_Thierry Draper_)
* 6168db8: Changes to get our Code Coverage up to 100% (_Thierry Draper_)

## Build v2.0.151 - d5ec6de - 2019-08-09 - CD: Move Server Details to Env Vars
* d5ec6de: Move the deploy server access details to environment variables (_Thierry Draper_)

## Build v2.0.150 - eb9bc2e - 2019-07-31 - Hotfix: Eager Load the ORM queries
* eb9bc2e: Minor variable naming fix within the site template (_Thierry Draper_)
* cc5da9b: Eager Load our page queries (_Thierry Draper_)

## Build v2.0.148 - 578d3e3 - 2019-07-21 - Composer: July 2019
* (Auto-commits only)

## Build v2.0.148 - 74889ae - 2019-07-17 - Feature: Monitor PHP Code Coverage
* 74889ae: Enable PHP code coverage report during CI (_Thierry Draper_)

## Build v2.0.147 - 3a58eed - 2019-07-10 - Hotfix: Skip Deploy in LocalCI
* 3a58eed: The new deploy stage should not be included in our localci run script (which also requires the script: rule to be last) (_Thierry Draper_)

## Build v2.0.146 - ed768bf - 2019-07-05 - Feature: Enable Continuous Delivery
* ed768bf: Envoy script for implementing our Continuous Delivery (_Thierry Draper_)

## Build v2.0.145 - ebbc9ef - 2019-06-18 - CI: PHPUnit vendor simplification
* ebbc9ef: May need to create the base CPAN CI dir (_Thierry Draper_)
* 7375beb: Fix, and where appropriate merge, vendor folder creation within the CI (_Thierry Draper_)
* 73979ca: Installing the skeleton for CI is a simpler process (_Thierry Draper_)
* 40fe61b: Ensure composer has a vendor folder to be installed into (_Thierry Draper_)
* 3f918f3: Propagate the CI_FAUX flag when running CI tests locally, but from clean (_Thierry Draper_)

## Build v2.0.140 - bed2e5d - 2019-06-17 - Hotfix: News Layout
* bed2e5d: News page item blocks inadvertently overlapping (_Thierry Draper_)

## Build v2.0.139 - 0baf044 - 2019-06-09 - SysAdmin: Sprites to CDN
* 0baf044: Move the sprites to the CDN (_Thierry Draper_)

## Build v2.0.138 - 3ec2cd5 - 2019-06-08 - CI: Handle Perl Upgrades
* 3ec2cd5: Handle changing versions of perl in the CI (_Thierry Draper_)

## Build v2.0.137 - ac6e28b - 2019-06-07 - CI: Data Download Errors
* ac6e28b: When downloading test CI data return an error if the download fails, rather than assume it was a 204/skip (_Thierry Draper_)

## Build v2.0.136 - 38a2bcc - 2019-05-21 - CI: Fix PHPUnit skeleton setup
* 38a2bcc: rsync management of PHPUnit test setup fix (_Thierry Draper_)

## Build v2.0.135 - d2b0e8f - 2019-05-02 - Migrations: Merge w/Stored Procedures
* d2b0e8f: Re-organise the database migrations layouts to keep all sub-grouped migrations/stored procs together (_Thierry Draper_)

## Build v2.0.134 - 0610817 - 2019-04-24 - Migrations: Connection-appropriate DROP TABLES
* 0610817: Migration "drop tables" needs appropriate Laravel connection applied (_Thierry Draper_)

## Build v2.0.133 - 2cf4999 - 2019-04-22 - CI: Testing Migrations
* 2cf4999: Include running the database migrations properly as part of the MySQL Linting CI job (plus testing their rollbacks) (_Thierry Draper_)

## Build v2.0.132 - db437b2 - 2019-04-16 - Composer: April 2019 updates
* db437b2: Some local 'clean' CI test fixes and code optimisations (_Thierry Draper_)

## Build v2.0.131 - 4e5c2ad - 2019-04-09 - CI: Fix Logging
* 4e5c2ad: Automatically fix the logs when cloning the skeleton within CI (_Thierry Draper_)

## Build v2.0.130 - f201708 - 2019-04-08 - CI: Check Migrations
* f201708: Add the migrations to our CI, including a PSR-12 workaround for Laravel's migrations (who do not follow PSR-4's fully qualified class name) (_Thierry Draper_)
* 733bd60: Re-jig how we run Laravel's migrations script should we sub-filter our migrations (_Thierry Draper_)

## Build v2.0.128 - 621c7b1 - 2019-03-28 - CI: Exclude Tags
* 621c7b1: Prevent CI when pushing tags (_Thierry Draper_)

## Build v2.0.127 - 4f2dc20 - 2019-03-26 - CI: Re-grouped Git remote
* 4f2dc20: Reflect re-grouping of the git remotes in our CI scripts (_Thierry Draper_)

## Build v2.0.126 - cf8eb1f - 2019-03-22 - CI: MySQL Linter
* cf8eb1f: Minor changes as part of importing a MySQL linter (_Thierry Draper_)

## Build v2.0.125 - bc3b52b - 2019-03-08 - Hotfix: Post Launch
* bc3b52b: Start using a singular composer cache to try and cut down on CI pipeline build time (_Thierry Draper_)
* 2bc48d2: Revise the way test databases are created, and include some post-test cleanup of them (_Thierry Draper_)
* 6a26552: Move the bulk of the CV content in to the CMS (_Thierry Draper_)
* 9e4ddaf: CI refinements to the Composer vendor caching rules and include a new Perl-linting process (_Thierry Draper_)
* fa32ea1: Make the unit tests compatible with some skeleton filesystem tweaks (_Thierry Draper_)
* 0ec1ff1: Test script tweaks to attempt repo-name parity between repo and skeleton (_Thierry Draper_)
* 990d829: Root-level merged folder isn't actually required (_Thierry Draper_)
* 63b2932: Fix incorrect $_SERVER variable name used on production (_Thierry Draper_)
* 122be26: Add symlinks required from live (_Thierry Draper_)

## Build v2.0.116 - a4d48b0 - 2019-02-17 - Launching Laravel - [![Version: 2.0](https://img.shields.io/badge/Version-2.0-brightgreen.svg)](https://gitlab.com/debear/www/tree/v2.0)
* a4d48b0: Git tidy pre merge of Laravel branch (_Thierry Draper_)
* 2963d5b: Make the VERSION info part of the repo (_Thierry Draper_)
* f384bd9: Some unit tests for the CV page too (_Thierry Draper_)
* 128231a: Update CV (_Thierry Draper_)
* 57669e7: Should be using 308, not 301 when redirecting after http:// to https:// transfer (_Thierry Draper_)
* 985e201: Minor CI tidying (_Thierry Draper_)
* 3e181cc: Use the new, standardised, way of loading the Font Awesome fonts (_Thierry Draper_)
* e517edf: PHPUnit setup script does need moving up in the CI YML (_Thierry Draper_)
* e15a926: Prune old MySQL seed data, plus build schema from migrations need downloaded seed (_Thierry Draper_)
* 118e938: Migrations for WWW (_Thierry Draper_)
* 03243d1: Regular Composer update (_Thierry Draper_)
* 67cf7cf: CI fixes (_Thierry Draper_)
* 1da657b: Missing MariaDB docker vars in the CI (_Thierry Draper_)
* 3a56d1b: Implement "feature" PHPUnit tests (_Thierry Draper_)
* 86ba6a0: Minor mark-up fixes (_Thierry Draper_)
* 05d411c: Refine the way sub-site routes are loaded (_Thierry Draper_)
* 6abef6e: CI composer fix to only install missing/out-of-date packages rather than update outside of composer.lock context (_Thierry Draper_)
* bf04c41: Include extended JS standards checking in the CI (_Thierry Draper_)
* 036cd4d: Apply TitleCase notation to JavaScript classes and camelCase notation to JavaScript methods (_Thierry Draper_)
* cf7e097: Script to run the CI tests locally (and accessing the composer libs directly, rather than through a git-runner) (_Thierry Draper_)
* e2abcc0: Add PSR and Pipeline Status flags to the README (_Thierry Draper_)
* d9f0704: Fallout from applying PSR-12 to the skeleton (_Thierry Draper_)
* 8ad492c: Fallout from applying PSR-12 to the skeleton (_Thierry Draper_)
* eaceff8: Add PSR-12 compliance testing to our CI (_Thierry Draper_)
* 209ba78: Minor PSR tidying following a phpcs run (_Thierry Draper_)
* 57feb80: Add some CSS and JS linting to our CI (_Thierry Draper_)
* 28c4023: PHPMD definition tweaks (_Thierry Draper_)
* 82b0465: Switch PHP we are testing against from 7.3 to 7.2 (_Thierry Draper_)
* c58f02a: Test of PHP Docker image weight for CI (_Thierry Draper_)
* 6dbf6e6: Third-party PHP Docker image missing curl (_Thierry Draper_)
* 15e90bb: Maybe a third-party Docker image? (_Thierry Draper_)
* 038ecea: Fixing PHP Zip CI issue by switching to Alpine? (_Thierry Draper_)
* 963e594: Latest attempt to get the CI working... (_Thierry Draper_)
* ce18ae3: PHP version issue in the CI (_Thierry Draper_)
* e686d1a: Some more iterative CI refinements (_Thierry Draper_)
* 9c2c2b0: Another minor missing CI dependency (_Thierry Draper_)
* d45258a: Fix Composer dependency issue in the CI (_Thierry Draper_)
* 7ccf9ff: Initial project CI (_Thierry Draper_)
* 9115797: Better model naming convention (_Thierry Draper_)
* d5e772a: Add definitions of the 1:1 relationships in our models (_Thierry Draper_)
* 3651e90: Latest version of the icon sprite from recent skeleton changes (_Thierry Draper_)
* 878fcbc: Remove "noopener noreferer" flags for internal pages to be opened in new tabs (_Thierry Draper_)
* 542db90: Minor LICENSE file tidying (_Thierry Draper_)
* 14aa314: Re-jigged iconset following a recent tidy (_Thierry Draper_)
* d6765a7: Tweaked iconset CSS naming (_Thierry Draper_)
* 6ad4b41: Meta descriptions for each page (_Thierry Draper_)
* 95321e8: Merge branch 'master' into laravel (_Thierry Draper_)
* b25d929: CV tweaks (_Thierry Draper_)
* cd15c6c: Remove the nav being loaded specifically by the subdomain, it's now part of the skeleton (_Thierry Draper_)
* 0fe9b5a: Latest iconset (_Thierry Draper_)
* da4f5bf: CV teaks (_Thierry Draper_)
* a2d3e8f: Move the config away from "GA" specifically to a more generic term (_Thierry Draper_)
* dff7253: Standardise the Sitemap and Nav links to the new model (_Thierry Draper_)
* 30c4a37: Missing symlink for the resulting merged JS files (_Thierry Draper_)
* b659d6d: Model tweaks to use our new customised base implementation (_Thierry Draper_)
* b72a0db: Tweaks based on recent refinements to the skeleton CSS (_Thierry Draper_)
* 88e6139: Minor Controller tidying (_Thierry Draper_)
* 8a97aa6: Mild CV tweaks (_Thierry Draper_)
* 313ce68: Re-jig of the News/Sites list to allow for responsiveness (_Thierry Draper_)
* d05fd90: Minor CSS pruning and tidying (_Thierry Draper_)
* d15c3d2: Include the nav JS/CSS globally (_Thierry Draper_)
* c97f024: Flags for setting the current navigation items in the Controllers (_Thierry Draper_)
* 0059cb1: New Icon Sprite for the general layout tweaks (_Thierry Draper_)
* 5105ed9: Tweak the news author getter to use the new numeric-ID-over-text-ID primary key (_Thierry Draper_)
* e67693d: Use correct User model class name (_Thierry Draper_)
* 4559f01: Switch our 301 redirects, to the more technically correct, 308 (preserving original HTTP method) (_Thierry Draper_)
* 14738eb: Effect of splitting out the DOM class into smaller components (_Thierry Draper_)
* 79e06ef: Introducing... v2! (_Thierry Draper_)
* 1efff0c: Minor tweak to site naming variable (_Thierry Draper_)
* a80368e: Sitemap / Navigation config details (_Thierry Draper_)
* 5b4b221: Tweaks for implementing the standard header/footer Blade templates around the site's pages (_Thierry Draper_)
* cd748d2: Minor blade template fixes, and duplicate route removal (_Thierry Draper_)
* f5ecca7: Initial .gitignore file (_Thierry Draper_)
* dd916d2: Tweaks from initial add of the DeBear middleware (_Thierry Draper_)
* 13453e1: Changes required for the CV page, including a re-jig of the resources for merging (_Thierry Draper_)
* 5609023: Added type hints to the methods and return values (_Thierry Draper_)
* 01d6854: Changes required for the /sites pages (_Thierry Draper_)
* 1a2a113: Proof-of-concept route, then transfer the homepage / news (_Thierry Draper_)
* 091b885: Converted About Us page (_Thierry Draper_)
* 5f634ca: Start of process of switching from Homebrew skeleton to Laravel (_Thierry Draper_)
* b52b49d: Removal of the 'hosted dev' environment on production (_Thierry Draper_)

## Build v1.0.33 - ad51ed2 - 2018-06-10 - Homebrew
* _Changelog truncated for initial build_
