<?php

/*
 * Routes for the www sub-domain
 */

// News.
Route::get('/', 'WWW\News@index');
Route::redirect('/html/', '/', 308);
Route::redirect('/html/index', '/', 308);
Route::redirect('/html/index.php', '/', 308);

// Sites.
Route::get('/sites', 'WWW\Sites@index');
Route::redirect('/sites/index', '/sites', 308);
Route::redirect('/sites/index.php', '/sites', 308);

// About.
Route::get('/about', 'WWW\About@index');
Route::redirect('/html/about', '/about', 308);

// CV - using our 'core' middelware.
Route::withoutMiddleware('process.web')->middleware('process.core')->group(function () {
    Route::get('/cv', 'WWW\CV@index');
    Route::get(
        '/cv/{slug}',
        function (string $slug) {
            return redirect("/cv?utm_source=DeBear&utm_medium=CV&utm_campaign=$slug");
        }
    );
});
