SET @sync_app := 'www';

#
# DeBear.co.uk PHP Database
#
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'DeBear.co.uk', 'dev', 'data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    (@sync_app, 1, '__DIR__ DeBear.co.uk CDN', 1, 'file'),
    (@sync_app, 3, '__DIR__ HOME_NEWS', 2, 'database'),
    (@sync_app, 4, '__DIR__ HOME_SITES', 3, 'database'),
    (@sync_app, 5, '__DIR__ HOME_SITES_SCREENSHOTS', 4, 'database');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt) VALUES
    (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/www', 'debear/sites/cdn/htdocs/www', NULL);
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    (@sync_app, 3, 'debearco_www', 'HOME_NEWS', ''),
    (@sync_app, 4, 'debearco_www', 'HOME_SITES', ''),
    (@sync_app, 5, 'debearco_www', 'HOME_SITES_SCREENSHOTS', '');
