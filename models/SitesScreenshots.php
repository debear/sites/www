<?php

namespace DeBear\Models\WWW;

use DeBear\Implementations\Model;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTTP;

class SitesScreenshots extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_www';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'HOME_SITES_SCREENSHOTS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'img_id';
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Cached compiled fully qualified site link
     * @var string
     */
    protected $full_url = '';

    /**
     * Build the URL to access the screenshot
     * @return string The full URL of the screenshot
     */
    public function fullUrl(): string
    {
        // Pre-cache.
        if (!$this->full_url) {
            $this->full_url = HTTP::buildDomain('static')
                . '/' . FrameworkConfig::get('debear.url.sub')
                . '/' . FrameworkConfig::get('debear.dirs.images')
                . '/news/scnsht_' . $this->src . '.png';
        }
        // Return.
        return $this->full_url;
    }
}
