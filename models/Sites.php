<?php

namespace DeBear\Models\WWW;

use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Implementations\Model;
use DeBear\Helpers\HTTP;

class Sites extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_www';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'HOME_SITES';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'site_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'launched' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Cached compiled fully qualified site link
     * @var string
     */
    protected $full_url = '';

    /**
     * Build the URL to access the site
     * @return string The full URL of the site
     */
    public function fullUrl(): string
    {
        // Pre-cache.
        if (!$this->full_url) {
            $this->full_url = HTTP::buildDomain($this->domain);
            if ($this->path) {
                $this->full_url .= '/' . $this->path;
            }
        }
        // Return.
        return $this->full_url;
    }

    /**
     * Relationship: 1:M screenshots
     * @return HasMany
     */
    public function screenshots(): HasMany
    {
        return $this->hasMany('DeBear\Models\WWW\SitesScreenshots', 'site_id', 'site_id');
    }
}
