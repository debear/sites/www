<?php

namespace DeBear\Models\WWW;

use Illuminate\Database\Eloquent\Relations\HasOne;
use DeBear\Implementations\Model;
use DeBear\Helpers\Articles;
use DeBear\Models\Skeleton\User;

class News extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_www';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'HOME_NEWS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'news_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'created' => 'datetime',
        'updated' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Determine the class(es) to apply to an article
     * @param integer $i    Article number on the page.
     * @param integer $page Current page number.
     * @return string CSS class(es)
     */
    public function listClass(int $i, int $page = 1): string
    {
        if (($page == 1 && $i) || $page > 1) {
            // Ideally half-width, unless responsive.
            return 'grid-1-2 grid-m-1-1 grid-tp-1-1';
        } else {
            // Exception is full-width.
            return 'grid-1-1';
        }
    }

    /**
     * Convert the article for display
     * @return string The article
     */
    public function render(): string
    {
        return Articles::render($this->article);
    }

    /**
     * Relationship: 1:1 User (who created the post)
     * @return HasOne
     */
    public function creator(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    /**
     * Relationship: 1:1 User (who last changed the post)
     * @return HasOne
     */
    public function updator(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'updated_by');
    }
}
