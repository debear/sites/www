<?php

/**
 * Config specifically for the www sub-domain
 */

return [
    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'home' => [
            'descrip' => "The latest news and updates from {$config['names']['site']}",
        ],
        // Sites.
        'sites' => [
            'title' => 'Our Sites',
            'descrip' => "All the sites in the {$config['names']['section']} group",
            'url' => '/sites',
            'order' => 30,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.8,
                'frequency' => 'monthly',
            ],
        ],
        // About.
        'about' => [
            'title' => 'About Us',
            'descrip' => 'A little information about us and our history',
            'url' => '/about',
            'order' => 40,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.6,
                'frequency' => 'yearly',
            ],
        ],
    ],

    /*
     * Site revision info
     */
    'version' => [
        'breakdown' => [
            'major' => 2,
            'minor' => 0,
        ],
    ],

    /*
     * Google Analytics
     */
    'analytics' => [
        'urchins' => [
            'tracking' => [ 'UA-48789782-1' ],
        ],
    ],
];
