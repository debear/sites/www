<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;

class HomeTest extends FeatureTestCase
{
    /**
     * A test of the main homepage
     * @return void
     */
    public function testHome(): void
    {
        // Home page on /, no pagination and paginated.
        foreach (['/', '/?page=1'] as $uri) {
            $response = $this->get($uri);
            $response->assertStatus(200);
            $response->assertSee('<h1>Welcome to DeBear.uk!</h1>');
            $response->assertSee('<h3>DeBear Fantasy Record Breakers in development</h3>');
            $response->assertDontSee('&laquo; First<span class="hidden-m"> Page</span>');
            $response->assertDontSee('&laquo; Prev<span class="hidden-m">ious Page</span>');
            $response->assertSee('Next<span class="hidden-m"> Page</span> &raquo;');
            $response->assertDontSee('Last<span class="hidden-m"> Page</span> &raquo;');
        }

        // Page 2.
        $response = $this->get('/?page=2');
        $response->assertStatus(200);
        $response->assertSee('<h1>Welcome to DeBear.uk!</h1>');
        $response->assertSee('<h3>DeBear Software re-brands with new-look domain</h3>');
        $response->assertSee('&laquo; First<span class="hidden-m"> Page</span>');
        $response->assertDontSee('&laquo; Prev<span class="hidden-m">ious Page</span>');
        $response->assertSee('Next<span class="hidden-m"> Page</span> &raquo;');
        $response->assertDontSee('Last<span class="hidden-m"> Page</span> &raquo;');

        // Page 3.
        $response = $this->get('/?page=3');
        $response->assertStatus(200);
        $response->assertSee('<h1>Welcome to DeBear.uk!</h1>');
        $response->assertSee('<h3>DeBear Sports MotoGP launched</h3>');
        $response->assertDontSee('&laquo; First<span class="hidden-m"> Page</span>');
        $response->assertSee('&laquo; Prev<span class="hidden-m">ious Page</span>');
        $response->assertDontSee('Next<span class="hidden-m"> Page</span> &raquo;');
        $response->assertSee('Last<span class="hidden-m"> Page</span> &raquo;');

        // Page 4.
        $response = $this->get('/?page=4');
        $response->assertStatus(200);
        $response->assertSee('<h1>Welcome to DeBear.uk!</h1>');
        $response->assertSee('<h3>DeBear Sports announced</h3>');
        $response->assertDontSee('&laquo; First<span class="hidden-m"> Page</span>');
        $response->assertSee('&laquo; Prev<span class="hidden-m">ious Page</span>');
        $response->assertDontSee('Next<span class="hidden-m"> Page</span> &raquo;');
        $response->assertDontSee('Last<span class="hidden-m"> Page</span> &raquo;');
    }

    /**
     * A test of the site list
     * @return void
     */
    public function testSites(): void
    {
        // Home page on /, no pagination and paginated.
        foreach (['/sites', '/sites?page=1'] as $uri) {
            $response = $this->get($uri);
            $response->assertStatus(200);
            $response->assertSee('<h1>DeBear.uk Websites</h1>');
            $response->assertSee('DeBear Sports: MLB');
            $response->assertDontSee('&laquo; First<span class="hidden-m"> Page</span>');
            $response->assertDontSee('&laquo; Prev<span class="hidden-m">ious Page</span>');
            $response->assertSee('Next<span class="hidden-m"> Page</span> &raquo;');
            $response->assertDontSee('Last<span class="hidden-m"> Page</span> &raquo;');
        }

        // Page 2.
        $response = $this->get('/sites?page=2');
        $response->assertStatus(200);
        $response->assertSee('<h1>DeBear.uk Websites</h1>');
        $response->assertSee('DeBear Sports: Speedway Grand Prix');
        $response->assertSee('&laquo; First<span class="hidden-m"> Page</span>');
        $response->assertDontSee('&laquo; Prev<span class="hidden-m">ious Page</span>');
        $response->assertDontSee('Next<span class="hidden-m"> Page</span> &raquo;');
        $response->assertSee('Last<span class="hidden-m"> Page</span> &raquo;');

        // Page 3.
        $response = $this->get('/sites?page=3');
        $response->assertStatus(200);
        $response->assertSee('<h1>DeBear.uk Websites</h1>');
        $response->assertSee('DeBear Sports: Giro d\'Italia');
        $response->assertDontSee('&laquo; First<span class="hidden-m"> Page</span>');
        $response->assertSee('&laquo; Prev<span class="hidden-m">ious Page</span>');
        $response->assertDontSee('Next<span class="hidden-m"> Page</span> &raquo;');
        $response->assertDontSee('Last<span class="hidden-m"> Page</span> &raquo;');
    }

    /**
     * A test of the About page
     * @return void
     */
    public function testAbout(): void
    {
        $response = $this->get('/about');
        $response->assertStatus(200);
        $response->assertSee('<h1>About DeBear.uk</h1>');
        $response->assertSee('<img class="profile" src="//static.debear.test/www/images/thierry.png" '
            . 'alt="Thierry Draper">');
    }

    /**
     * A test of the manifest.json file
     * @return void
     */
    public function testManifest(): void
    {
        $response = $this->get('/manifest.json');
        $response->assertStatus(200);
        $response->assertJson([
            'name' => 'DeBear.uk',
            'short_name' => 'DeBear.uk',
            'description' => 'The latest news and updates from DeBear.uk',
            'start_url' => '/?utm_source=homescreen',
            'icons' => [[
                'src' => 'https://static.debear.test/skel/images/meta/debear.png',
                'sizes' => '512x512',
                'type' => 'image/png',
            ]],
        ]);
    }
}
