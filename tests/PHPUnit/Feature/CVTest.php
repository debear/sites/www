<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;

class CVTest extends FeatureTestCase
{
    /**
     * A test of the CV
     * @return void
     */
    public function testCV(): void
    {
        // Main web version.
        $response = $this->get('/cv');
        $response->assertStatus(200);
        $response->assertSee('<title>Curriculum Vitae &ndash; Thierry Draper</title>');
        $response->assertSee('<dd><span class="enom">hello</span><span class="edenom">debear.uk</span></dd>');
        $response->assertDontSee('<dt><i class="fas fa-mobile-alt"></i>Telephone:</dt>');

        // PDF version.
        $response = $this->get('/cv?pdf=on');
        $response->assertStatus(200);
        $response->assertSee('<title>Curriculum Vitae &ndash; Thierry Draper</title>');
        $response->assertSee('<dt><i class="fas fa-mobile-alt"></i>Telephone:</dt>');

        // Redirects.
        $response = $this->get('/cv/redirect');
        $response->assertRedirect('/cv?utm_source=DeBear&utm_medium=CV&utm_campaign=redirect');

        $response = $this->get('/cv?utm_source=DeBear&utm_medium=CV&utm_campaign=redirect');
        $response->assertStatus(200);
        $response->assertSee(
            'An online version of this CV can be found at <a href="https://debear.uk/cv/redirect" '
            . 'target="_blank">https://debear.uk/cv/redirect</a>.'
        );

        // Merged CSS.
        $response = $this->get('/cv?merge-css=true');
        $response->assertStatus(200);

        // Re-directs non-SSL traffic.
        $this->toggleConfig('ssl'); // Consider non-SSL.
        $response = $this->get('/cv');
        $response->assertStatus(308);
        $this->toggleConfig('ssl'); // Re-enable SSL.
    }
}
