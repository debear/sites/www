# Helper methods for interacting with the GitLab API

api_key=$(cat $dir_base/../../../../etc/passwd/api/gitlab/sast-reports)
api_base='https://gitlab.com/api/v4/projects'

# Determine the project aspect to the API path
project_path=$(git config --get remote.origin.url | sed -r -e 's/^.+\.com:(.+)\.git$/\1/' -e 's/\//%2F/g')
api_base="$api_base/$project_path"

# Calling wrapper method
function call_gitlab_api() {
    api_endpoint="$1"
    outfile="$2"

    # If we've already got the file on-disk, we don't need to make the request
    [ -e $outfile ] && return

    # Make the requesst, returning the response code on STDOUT for later parsing
    ret_code=$(curl --silent --location \
        --user-agent 'DeBear Requestor/1.0' \
        --header 'Accept-Encoding: gzip' \
        --header "PRIVATE-TOKEN: $api_key" \
        --output $outfile \
        --write-out '%{response_code}' \
        "$api_base$api_endpoint")
    # If the request returned an error, abort
    if [ $(echo "$ret_code" | grep -Pc '^[23]') -eq 0  ]
    then
        display_error -n "GitLab API call to '$api_endpoint' returned HTTP Status $ret_code" >&2
        abort_download
        exit 1
    fi
}
