@servers(['deployer' => 'deployer'])

@task('deploy', ['on' => 'deployer'])
    {{-- Dealt with by our local script, so tell it which repo and which commit to release --}}
    deploy-repo "{{ $repo }}" "{{ $commit }}"
@endtask

