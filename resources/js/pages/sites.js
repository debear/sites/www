get('dom:load').push(() => {
    DOM.children('.img.onclick_target').forEach((ele) => {
        Events.attach(ele, 'click', () => {
            var eleImg = ele.closest('.img_inner').querySelector('img.main');
            var eleThumb = ele.querySelector('img');
            eleImg.alt = eleThumb.alt;
            eleImg.src = eleThumb.src;
        });
    });
});
